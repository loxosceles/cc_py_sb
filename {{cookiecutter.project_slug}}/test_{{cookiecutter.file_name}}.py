#!/usr/bin/env python3

import pytest
from {{ cookiecutter.file_name }} import func

@pytest.mark.parametrize("args, expected", [
    (("arg1", "arg2"), True),
    #  ((arg1, arg2), 4),
])
def test_func(args, expected):
    arg1 = args[0]
    arg2 = args[1]
    assert func(arg1, arg2) == expected
